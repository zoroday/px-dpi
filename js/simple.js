document.write("<script language='javascript' src='js/px2xx.js'></script>");

var ldpi_PPI = 120.0;
var mdpi_PPI = 160.0;
var hdpi_PPI = 240.0;
var xhdpi_PPI = 320.0;
var xxhdpi_PPI = 480.0;

function ldpi_dp(px) {
	return px2dip(px, ldpi_PPI);
}

function ldpi_sp(px) {
	return px2sp(px, ldpi_PPI);
}

function mdpi_dp(px) {
	return px2dip(px, mdpi_PPI);
}

function mdpi_sp(px) {
	return px2sp(px, mdpi_PPI);
}

function hdpi_dp(px) {
	return px2dip(px, hdpi_PPI);
}

function hdpi_sp(px) {
	return px2sp(px, hdpi_PPI);
}

function xhdpi_dp(px) {
	return px2dip(px, xhdpi_PPI);
}

function xhdpi_sp(px) {
	return px2sp(px, xhdpi_PPI);
}

function xxhdpi_dp(px) {
	return px2dip(px, xxhdpi_PPI);
}

function xxhdpi_sp(px) {
	return px2sp(px, xxhdpi_PPI);
}