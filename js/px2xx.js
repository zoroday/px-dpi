
function initPPI(width, height, diagonal) {
	var ppi = 0;
	if (diagonal > 0) {
		var ppi2 = width * width + height * height;	
		ppi = Math.pow(ppi2, 0.5) / diagonal;
		ppi = Math.round(ppi);
	}
	return ppi;
}

function px2dip(px, ppi) {
	var dp = 0;
	if (ppi > 0) {
		dp = px * 160 / ppi;
	}
	
	return dp;
}	

function px2sp(px, ppi) {
	var sp = 0;
	if (ppi > 0) {
		sp = px * 160 / ppi;
	}
	return sp;
}